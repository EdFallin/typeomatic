/**/

import {Component, OnInit} from '@angular/core';
import {Output, EventEmitter} from '@angular/core';
/* As in model code, services used by the component have to be
   imported here, whether they use HttpClient or otherwise.  */
import {UxStateService} from '../ux-state.service';
import {TypingService} from '../typing.service';

@Component({
  selector: 'app-chooser',
  templateUrl: './chooser.component.html',
  styleUrls: ['./chooser.component.css']
})
export class ChooserComponent implements OnInit {
  isHidden: boolean = true;

  textNames: string[] = [];
  @Output() onNameChosen = new EventEmitter<string>();

  constructor(private stateService: UxStateService, private typingService: TypingService) {
    this.stateService.stateIsReady
      .subscribe(() => {
        this.isHidden = this.stateService.isChooserHidden;
      });

    this.typingService.getAllNames()
      .subscribe((names) => {
        this.textNames = names;
      });
  }

  ngOnInit(): void {
  }

  whenToggleChooserClicked() {
    this.isHidden = !this.isHidden;
    this.stateService.isChooserHidden = this.isHidden;
  }

  whenNameButtonClicked(e: Event) {
    let sender = e.target as Element;
    let id = sender.id;

    if (this.textNames.includes(id)) {
      this.onNameChosen.emit(id);
    }
  }
}
