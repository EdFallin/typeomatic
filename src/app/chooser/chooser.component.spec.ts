/**/

/* When code to be tested has injected dependencies, these are necessary. */
import {ComponentFixture, TestBed} from '@angular/core/testing';
/* To test code with an HttpClient dependency somewhere, you import
   this type instead of the model type, from a subfolder of /http. */
import {HttpClientTestingModule} from '@angular/common/http/testing';
/* To test code using observables, you need one or both of these. */
import {Observable, of as Of} from 'rxjs';

import {ChooserComponent} from './chooser.component';
import {UxStateService} from '../ux-state.service';
import {TypingService} from '../typing.service';

describe('ChooserComponent', () => {
  let component: ChooserComponent;
  let harness: ComponentFixture<ChooserComponent>;
  let service = {
    getAllNames() {
      return Of(['First', 'Second', 'Third']);
    },
  };

  beforeEach(async () => {
    /* Tests have their own module, created imperatively here at runtime.  It takes the place of the app module,
       including imports for externals, declarations for internals, and providers for (internal) services.  */
    await TestBed.configureTestingModule({
      /* To test code with an HttpClient dependency somewhere, you add the special test
         HTTP type to the test module's imports, since you're getting it from Angular.
         It's useful to think of HttpClient and similar just as types, not as services. */
      imports: [HttpClientTestingModule],
      /* The component to be tested is listed in declarations, along with any
         components (but not importeds or services) it interacts with in tests. */
      declarations: [ChooserComponent],
      /* You add your own services in providers, even if they use HttpClient. */
      providers: [
        UxStateService,
        {
          provide: TypingService,
          useValue: service
        }
      ]
    })
      /* You have to compile the components to include and use any of their
         HTML-facing aspects, but also to have all dependencies loadable. */
      .compileComponents();
  });

  beforeEach(() => {
    /* Although this instance's type is called a "fixture" by Angular, it's really
       a test harness, which even manages inter-component interactions when needed. */
    harness = TestBed.createComponent(ChooserComponent);
    /* Before each test, a reusable instance is retrieved this way.  Injection of
       providers and any imported types (like HttpClient) is done invisibly for you. */
    component = harness.componentInstance;
    /* This call defines the harness symbol as representing a harness,
       because the instance manages interaction among tested peers. */
    harness.detectChanges();
  });

  /* working */
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /* working */
  it('should provide names from service as .textNames', async () => {
    /* Prepare. */
    let expected = ['First', 'Second', 'Third'];

    // Enacted automatically.

    /* Compare. */
    let actual = component.textNames;
    expect(actual).toEqual(expected);
  });

  /* working */
  it('should display names from service onscreen', async () => {
    /* Prepare. */
    let dom = harness.nativeElement;
    let expecteds = ['First', 'Second', 'Third'];

    // Enacted automatically.

    /* Compare. */
    for (let expected of expecteds) {
      let actual = dom.querySelector(`#${expected}`).id;
      expect(actual).toBeDefined();
    }
  });

  /* ?? */
  it('should raise onNameChosen with named text when its button is clicked',
    async () => {
      /* Prepare. */
      let name = component.textNames[0];
      let dom = harness.nativeElement;
      let button: HTMLButtonElement = dom.querySelector(`#${name}`);
      let expected = name;

      let didRun = false;

      // Defining a comparing handler.
      component.onNameChosen.subscribe((actual: string) => {
        expect(actual).toBe(expected);
        didRun = true;
      });

      /* Enact. */
      button.click();
      harness.detectChanges();

      /* Compare. */
      expect(didRun).toBeTrue();
    });
});
