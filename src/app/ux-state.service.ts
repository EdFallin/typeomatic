/**/

import {Injectable} from '@angular/core';
import {TypeStyle} from './type-style';
import {UxState} from './ux-state';
import { Output, EventEmitter } from "@angular/core";
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UxStateService {
  private state: UxState;

  @Output() stateIsReady : EventEmitter<void>;

  constructor(private http: HttpClient) {
    this.stateIsReady = new EventEmitter<void>();

    // Now any saved state from the server.
    this.getState();
  }

  getState() {
    /* Cruft: The subscribe() block should handle HTTP status codes.  */
    this.http.get<UxState>('https://TypeOMaticServer.azurewebsites.net/UxState')
      .subscribe((result: any) => {
        this.state = result;
        this.stateIsReady.emit();
      }, (error) => {
        console.log(error);
      });
  }

  get isWelcomeHidden(): boolean {
    return this.state.isWelcomeHidden;
  }

  set isWelcomeHidden(x: boolean) {
    this.state.isWelcomeHidden = x;
    this.setState();
  }

  get isTimerHidden(): boolean {
    return this.state.isTimerHidden;
  }

  set isTimerHidden(x: boolean) {
    this.state.isTimerHidden = x;
    this.setState();
  }

  get isChooserHidden(): boolean {
    return this.state.isChooserHidden;
  }

  set isChooserHidden(x: boolean) {
    this.state.isChooserHidden = x;
    this.setState();
  }

  get typingStyle(): TypeStyle {
    return this.state.typingStyle;
  }

  set typingStyle(x: TypeStyle) {
    this.state.typingStyle = x;
    this.setState();
  }

  setState() {
    // HttpClient.post() is really a declaration of code that returns an Observable.
    // To actually POST, we have to subscribe() to ask the Observable to be generated.
    // The subscribe() call doesn't necessarily have to do anything, though.
    this.http.post('https://TypeOMaticServer.azurewebsites.net/UxState', this.state)
      /* Cruft: The subscribe() block should handle HTTP status codes. */
      .subscribe();
  }
}
