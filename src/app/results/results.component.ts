/**/

import { Component, OnInit } from '@angular/core';
import { ResultBundle } from "../resultbundle";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  speed: string;
  right: string;
  typos: string;

  constructor() {
    this.setDefaultTexts();
  }

  ngOnInit(): void {
  }

  setDefaultTexts() {
    this.speed = "80";
    this.right = "100";
    this.typos = "0";
  }

  calculateResults(results: ResultBundle) {
    let speed = this.calculateSpeed(results);
    this.speed = speed.toFixed(0);

    let right = this.calculateRight(results);
    this.right = right.toFixed(1);

    let typos = this.calculateTypos(results);
    this.typos = typos.toFixed(1);
  }

  calculateSpeed(results: ResultBundle) : number {
    /* standard net wpm formula */

    let { typed, minutes, typos } = results;

    let speed = ((typed / 5) - typos) / minutes;
    return speed;
  }

  calculateRight(results: ResultBundle) : number {
    /* standard accuracy formula */

    let { characters, mistakes } = results;

    let right = ((characters - mistakes) / characters) * 100;
    return right;
  }

  calculateTypos(results: ResultBundle) : number {
    /* custom "net mistakes" formula */

    let { typed, mistakes } = results;

    let typos = (mistakes / typed) * 100;
    return typos;
  }
}
