/**/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing";

import { TyperComponent } from './typer.component';
import { UxStateService } from "../ux-state.service";

describe('TyperComponent', () => {
  let component: TyperComponent;
  let harness: ComponentFixture<TyperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [ TyperComponent ],
      providers: [ UxStateService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    harness = TestBed.createComponent(TyperComponent);
    component = harness.componentInstance;
    harness.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
