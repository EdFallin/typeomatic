import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ReaderComponent} from './reader/reader.component';
import {TyperComponent} from './typer/typer.component';
import {ChooserComponent} from './chooser/chooser.component';
import {OvertyperComponent} from './overtyper/overtyper.component';
import {ResultsComponent} from './results/results.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {TimerComponent} from './timer/timer.component';
import {UxStateService} from './ux-state.service';
import {TypingService} from './typing.service';
import {HttpClientModule} from '@angular/common/http';
import { TimeEnderComponent } from './time-ender/time-ender.component';

@NgModule({
  declarations: [
    AppComponent,
    ReaderComponent,
    TyperComponent,
    ChooserComponent,
    OvertyperComponent,
    ResultsComponent,
    WelcomeComponent,
    TimerComponent,
    TimeEnderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [
    UxStateService,
    TypingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
