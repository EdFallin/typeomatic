/**/

/* To test a service, you need both the TestBed and inject, both from the same site. */
import {TestBed, inject} from '@angular/core/testing';

/* The HTTP testing module is needed here because the service uses HttpClient. */
import {HttpClientTestingModule} from '@angular/common/http/testing';

import {TypingService} from './typing.service';

describe('TypingService', () => {
  /* This before-each can be synchronous, because it's not compiling. */
  beforeEach(() => {
    TestBed.configureTestingModule({
      /* The HTTP testing module goes here, as with other tests. */
      imports: [HttpClientTestingModule],
      /* The service to be tested goes in .providers, just as if it were a dependency. */
      providers: [TypingService]
    });
  });

  it(
    'should be created',
    /* Instead of the usual () => { }, the test's body is inject([ServiceName],  ...),
       with a nested () => { }, except now with an arg symbol for the injected service */
    inject(
      [TypingService],
      (service) => {
        expect(service).toBeTruthy();
      }
    )
  );
});
