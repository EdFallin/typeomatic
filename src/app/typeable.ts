/**/

export type Typeable = {
  name: string,
  text: string,
  wordCount: number,
  minuteLength: number
};

