import {Component, OnInit, Injectable} from '@angular/core';
import {Output, EventEmitter} from '@angular/core';
import {UxStateService} from '../ux-state.service';
import {TypeStyle} from '../type-style';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  isHidden: boolean = true;

  @Output() readAndTypeWasClicked: EventEmitter<void>;
  @Output() typeOverWasClicked: EventEmitter<void>;

  constructor(private stateService: UxStateService) {
    this.readAndTypeWasClicked = new EventEmitter<void>();
    this.typeOverWasClicked = new EventEmitter<void>();

    this.stateService.stateIsReady
      .subscribe(() => {
        this.isHidden = this.stateService.isWelcomeHidden;
      });
  }

  ngOnInit(): void {
  }

  whenReadAndTypeClicked() {
    this.readAndTypeWasClicked.emit();
    this.stateService.typingStyle = TypeStyle.ReadAndType;
  }

  whenTypeOverClicked() {
    this.typeOverWasClicked.emit();
    this.stateService.typingStyle = TypeStyle.TypeOver;
  }

  whenToggleWelcomeClicked() {
    this.isHidden = !this.isHidden;
    this.stateService.isWelcomeHidden = this.isHidden;
  }
}
