/* Needed for dependency injection. */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/* To test HttpClient consumers, folder-leafward of HttpClient itself. */
import { HttpClientTestingModule } from "@angular/common/http/testing";

/* To get the code under test and the service it uses (which uses HttpClient). */
import { WelcomeComponent } from './welcome.component';
import { UxStateService } from "../ux-state.service";

describe('WelcomeComponent', () => {
  /* Regional instances of the model code and
     a test harness (to Angular, a "fixture"). */
  let component: WelcomeComponent;
  let harness: ComponentFixture<WelcomeComponent>;

  beforeEach(async () => {
    /* Defining the test module, paralleling an app module. */
    await TestBed.configureTestingModule({
      /* The externals, including HttpClient equivalent. */
      imports: [ HttpClientTestingModule ],
      /* Target model code and sometimes its interaction peers. */
      declarations: [ WelcomeComponent ],
      /* Services which must be injected. */
      providers: [ UxStateService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    /* Each test gets a fresh version of the full component. */
    harness = TestBed.createComponent(WelcomeComponent);
    component = harness.componentInstance;
    /* This is how we know it's a harness: it is managing interactions here. */
    harness.detectChanges();
  });

  it('should create', () => {
    /* The test of a simple basic for any component class.
       Injection was already done invisibly in beforeEach(). */
    expect(component).toBeTruthy();
  });
});
