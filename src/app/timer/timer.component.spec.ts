/**/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing";

import { TimerComponent } from './timer.component';
import { UxStateService } from "../ux-state.service";
import { TypingService } from "../typing.service";

describe('TimerComponent', () => {
  let component: TimerComponent;
  let harness: ComponentFixture<TimerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [ TimerComponent ],
      providers: [ UxStateService, TypingService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    harness = TestBed.createComponent(TimerComponent);
    component = harness.componentInstance;
    harness.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
