/**/

import {Component, OnInit, Output, EventEmitter } from '@angular/core';
import {UxStateService} from '../ux-state.service';
import { TypingService } from "../typing.service";

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  isHidden: boolean = true;

  timesAndTexts: { time: number, text: string }[] = [];

  @Output() onTimeButtonClick = new EventEmitter<number>();

  constructor(private stateService: UxStateService, private typingService: TypingService) {
    this.stateService.stateIsReady
      .subscribe(() => {
        this.isHidden = this.stateService.isTimerHidden;
      });

    this.typingService.getMinutesPossible()
      .subscribe((times) => {
        for (let time of times) {
          let text = `${ time } minute${ time != 1 ? "s" : "" }`;
          this.timesAndTexts.push({ time: time, text: text });
        }
      });
  }

  ngOnInit(): void {
  }

  whenToggleTimerClicked() {
    this.isHidden = !this.isHidden;
    this.stateService.isTimerHidden = this.isHidden;
  }

  whenTimeButtonClicked(e : Event) {
    let sender = e.target as Element;
    let minutes = Number(sender.id);
    this.onTimeButtonClick.emit(minutes);
  }
}
