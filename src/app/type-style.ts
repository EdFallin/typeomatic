export enum TypeStyle {
  ReadAndType = 'ReadAndType',
  TypeOver = 'TypeOver',
  TypeNew = 'TypeNew'
}
