/**/

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';

import {AppComponent} from './app.component';


import {Mediator} from './mediator';

// The component has to know all the app's components' types to
// retain them as view children and provide them to the Mediator.
import {ChooserComponent} from './chooser/chooser.component';
import {OvertyperComponent} from './overtyper/overtyper.component';
import {ReaderComponent} from './reader/reader.component';
import {ResultsComponent} from './results/results.component';
import {TimerComponent} from './timer/timer.component';
import {TyperComponent} from './typer/typer.component';
import {WelcomeComponent} from './welcome/welcome.component';

import {TypingService} from './typing.service';
import {UxStateService} from './ux-state.service';

describe('AppComponent', () => {
  let harness: ComponentFixture<AppComponent>;
  let app: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [
        AppComponent,
        ChooserComponent,
        TimerComponent,
        ReaderComponent,
        OvertyperComponent
      ],
      providers: [
        UxStateService,
        TypingService
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    harness = TestBed.createComponent(AppComponent);
    app = harness.componentInstance;
    harness.detectChanges();
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have as title "TypeOMatic"`, () => {
    expect(app.title).toEqual('TypeOMatic');
  });

  it('should render title', () => {
    let compiled = harness.nativeElement;

    /* The arg to querySelector() uses the same syntax as CSS selectors. */
    expect(compiled.querySelector('h1.title').textContent).toContain('Type-O-Matic');
  });
});
