/**/

import { Component, OnInit } from '@angular/core';
import { Text } from "../text";

@Component({
  selector: 'app-reader',
  templateUrl: './reader.component.html',
  styleUrls: ['./reader.component.css']
})
export class ReaderComponent implements OnInit {
  textToType : string = "Reader.";
  minutes: number;

  constructor() {
    /* No operations. */
  }

  ngOnInit(): void {
  }

  whenTextIsAvailable(text : string, minutes: number) {
    this.textToType = text;
    this.minutes = minutes;
  }
}
