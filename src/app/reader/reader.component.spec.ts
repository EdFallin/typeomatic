/**/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing";

import { ReaderComponent } from './reader.component';
import { UxStateService } from "../ux-state.service";

describe('ReaderComponent', () => {
  let component: ReaderComponent;
  let harness: ComponentFixture<ReaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [ ReaderComponent ],
      providers: [ UxStateService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    harness = TestBed.createComponent(ReaderComponent);
    component = harness.componentInstance;
    harness.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
