/**/

import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {TypeStyle} from './type-style';
import {UxStateService} from './ux-state.service';
import { TypingService } from "./typing.service";

import {Mediator} from './mediator';

// The component has to know all the app's components' types to
// retain them as view children and provide them to the Mediator.
import {ChooserComponent} from './chooser/chooser.component';
import {OvertyperComponent} from './overtyper/overtyper.component';
import {ReaderComponent} from './reader/reader.component';
import {ResultsComponent} from './results/results.component';
import {TimerComponent} from './timer/timer.component';
import {TyperComponent} from './typer/typer.component';
import {WelcomeComponent} from './welcome/welcome.component';
import { TimeEnderComponent } from "./time-ender/time-ender.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'TypeOMatic';

  typeStyle: TypeStyle = TypeStyle.ReadAndType;

  mediator: Mediator;

  // The types must be available as view children to be provided to the Mediator.
  @ViewChild(ChooserComponent, {static: true}) chooser: ChooserComponent;
  @ViewChild(OvertyperComponent, {static: true}) overtyper: OvertyperComponent;
  @ViewChild(ReaderComponent, {static: true}) reader: ReaderComponent;
  @ViewChild(ResultsComponent, {static: true}) results: ResultsComponent;
  @ViewChild(TimerComponent, {static: true}) timer: TimerComponent;
  @ViewChild(TyperComponent, {static: true}) typer: TyperComponent;
  @ViewChild(WelcomeComponent, {static: true}) welcome: WelcomeComponent;
  @ViewChild(TimeEnderComponent, {static: true}) timeEnder: TimeEnderComponent;

  constructor(private stateService: UxStateService, private typingService: TypingService) {
    this.stateService.stateIsReady
      .subscribe(() => {
        this.typeStyle = this.stateService.typingStyle;
      });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // Initing the Mediator to delegate control of
    // interactions to it, to keep this class simple.
    this.initMediator();
  }

  initMediator() {
    this.mediator = new Mediator(
      this.typingService,
      this.chooser,
      this.overtyper,
      this.reader,
      this.results,
      this.timer,
      this.typer,
      this.welcome,
      this.timeEnder
    );
  }

  whenWelcomeTypeOverClicked() {
    this.typeStyle = TypeStyle.TypeOver;
  }

  whenWelcomeReadAndTypeClicked() {
    this.typeStyle = TypeStyle.ReadAndType;
  }

  get DoHideTypeOver(): boolean {
    return this.typeStyle !== TypeStyle.TypeOver;
  }

  get DoHideReadAndType(): boolean {
    return this.typeStyle !== TypeStyle.ReadAndType;
  }
}
