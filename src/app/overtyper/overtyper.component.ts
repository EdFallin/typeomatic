/**/

import {Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import {ResultBundle} from '../resultbundle';
import {Text} from '../text';

@Component({
  selector: 'app-overtyper',
  templateUrl: './overtyper.component.html',
  styleUrls: ['./overtyper.component.css']
})
export class OvertyperComponent implements OnInit {
  textToType: string = 'Overtyper.';
  at: number;
  wasShifted: boolean;

  none = -1;

  shift = 'Shift';

  // These keys are always ignored by key-down and key-up.
  ignorables = [
    'ArrowUp', 'ArrowLeft', 'ArrowDown', 'ArrowRight',
    'Shift', 'Control', 'Alt', 'Meta', 'Delete'
  ];

  // These keys are handled differently by Firefox on
  // key-down, so this list is used to override that.
  problems = [' ', '\'', '/'];

  // This key is also ignored on key-down.
  backspacers = ['Backspace'];

  // Gathers all keys to be ignored on key-down.
  metas: string[];

  // Styles for typed characters, to simplify restyling.
  removables = ['good', 'bad', 'next'];

  // Values used in calculating the various results.  Individual typos are
  // tracked individually for WPM, since they can be fixed, but all chars
  // and mistakes are also tracked for two other result measurements.
  timeWasStarted: boolean = false;
  minutes: number;
  characters: number;
  typos: number[];
  mistakes: number;

  typingTimer;

  // The DOM in a form accessible by plain JS here.
  dom: Document;
  typeArea: HTMLDivElement;

  // Events used by the Mediator and downstream consumers.
  @Output() onTimeWasCompleted: EventEmitter<ResultBundle> = new EventEmitter<ResultBundle>();

  constructor() {
    this.dom = document;
    this.metas = [...this.ignorables, ...this.problems, ...this.backspacers];
  }

  ngOnInit(): void {
  }

  whenTextIsAvailable(text: string, minutes: number) {
    this.typeArea = this.dom.getElementById('OvertyperTypeArea') as HTMLDivElement;
    this.typeArea.innerHTML = null;

    this.textToType = text;
    this.at = 0;
    this.characters = 0;
    this.typos = [];
    this.mistakes = 0;

    this.minutes = minutes;
    this.timeWasStarted = false;

    for (let char of this.textToType) {
      this.addUntypedChar(char);
    }
  }

  addUntypedChar(char: string) {
    let span = this.dom.createElement('SPAN');
    span.innerText = char;
    span.classList.add('char');
    this.typeArea.appendChild(span);
  }

  whenClicked(e: MouseEvent) {
    if (this.at == 0 && this.typeArea.childNodes.length > 0) {
      this.styleCharNext(0);
    }
  }

  whenKeyDown(e: KeyboardEvent) {
    let key = e.key;

    // Allowing solo shift key presses through, and setting a flag
    // to allow emulating how keyboards let you ease up shift early.
    if (key === this.shift) {
      this.wasShifted = true;
      return;
    }

    // Block automatic responses to special keys
    // and to keys browsers may use specially.
    if (this.metas.includes(key)) {
      e.preventDefault();
      e.stopPropagation();
    }
  }

  whenKeyUp(e: KeyboardEvent) {
    e.preventDefault();
    e.stopPropagation();

    let key = e.key;

    // Ignore combination keys that are included
    // in the key stream as though regular keys.
    if (this.ignorables.includes(key)) {
      return;
    }

    // Imitate backspacing when backspace is pressed.
    if (this.backspacers.includes(key)) {
      this.whenBackspacing(e);
      return;
    }

    // Start timer at first normal typing.
    if (!this.timeWasStarted) {
      this.whenTimeStarting();
    }

    // Imitate match-aware typing on all other keys.
    this.whenTyping(e);
  }

  whenBackspacing(e: KeyboardEvent) {
    this.at--;
    this.at = Math.max(0, this.at);

    this.clearAnyTypo();

    this.styleCharNext(this.at);
    this.unstyleChar(this.at + 1);
  }

  whenTimeStarting() {
    // Timers are in milliseconds, so * sec / min * msec / sec.
    let msecs = this.minutes * 60 * 1000;

    // Setting the end-of-typing responder.
    this.typingTimer = setTimeout(() => {
      this.whenTimedOut();
    }, msecs);

    // A timer should only be set once each time.
    this.timeWasStarted = true;
  }

  whenTimedOut() {
    // For WPM, finished text is counted.
    // Final counter is 1 past the end.
    let top = this.at - 1;

    // Gathering results to emit.
    let results: ResultBundle = {
      typed: top,
      characters: this.characters,
      minutes: this.minutes,
      typos: this.typos.length,
      mistakes: this.mistakes
    };

    // Emitting the event with the results,
    // and resetting some local state.
    this.onTimeWasCompleted.emit(results);
    clearTimeout(this.typingTimer);
    this.typingTimer = null;
  }

  whenTyping(e: KeyboardEvent) {
    let key = e.key;
    let char = this.textToType[this.at];

    // For accuracy measures, all typed chars (except metas) are counted.
    this.characters++;

    // The right char was typed.
    if (key === char) {
      this.styleCharGood(key, char);
    }
    // The right capital char was typed, but the shift key was released a little too early.
    // Keyboards normally let you get away with this, so it has to be imitated here.
    else if (this.wasShifted && key === char.toLowerCase()) {
      this.styleCharGood(key, char);
    }
    // The wrong char was typed.
    else {
      this.styleCharBad(key, char);
      this.mistakes++;
      this.typos.push(this.at);
    }

    this.wasShifted = false;

    this.at++;
    this.styleCharNext(this.at);
  }

  clearAnyTypo() {
    let index = this.typos.indexOf(this.at);

    if (index != this.none) {
      this.typos.splice(index, 1);
    }
  }

  styleCharGood(key: string, char: string) {
    let span = this.typeArea.childNodes[this.at] as HTMLSpanElement;
    span.classList.remove(...this.removables);
    span.classList.add('good');
  }

  styleCharBad(key: string, char: string) {
    let span = this.typeArea.childNodes[this.at] as HTMLSpanElement;
    span.classList.remove(...this.removables);
    span.classList.add('bad');
  }

  styleCharNext(at: number) {
    let nextSpan = this.typeArea.childNodes[at] as HTMLSpanElement;
    nextSpan.classList.remove(...this.removables);
    nextSpan.classList.add('next');
  }

  unstyleChar(at: number) {
    let afterSpan = this.typeArea.childNodes[at] as HTMLSpanElement;
    afterSpan.classList.remove(...this.removables);
  }
}
