/**/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing";

import { OvertyperComponent } from './overtyper.component';
import { UxStateService } from "../ux-state.service";

describe('OvertyperComponent', () => {
  let component: OvertyperComponent;
  let harness: ComponentFixture<OvertyperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [ OvertyperComponent ],
      providers: [ UxStateService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    harness = TestBed.createComponent(OvertyperComponent);
    component = harness.componentInstance;
    harness.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
