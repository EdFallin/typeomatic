import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeEnderComponent } from './time-ender.component';

describe('TimeEnderComponent', () => {
  let component: TimeEnderComponent;
  let fixture: ComponentFixture<TimeEnderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeEnderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeEnderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
