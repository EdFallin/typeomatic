import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-time-ender',
  templateUrl: './time-ender.component.html',
  styleUrls: ['./time-ender.component.css']
})
export class TimeEnderComponent implements OnInit {
  shouldBeDisplayed = false;

  constructor() { }

  ngOnInit(): void {
  }

  onOkClick() {
    this.shouldBeDisplayed = false;
  }

}
