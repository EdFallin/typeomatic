/**/

import {TestBed, ComponentFixture} from '@angular/core/testing';
import {Observable, of as Of} from 'rxjs';

import {Mediator} from './mediator';
import {Text} from './text';

// The Mediator knows all its colleagues' types.
import {ChooserComponent} from './chooser/chooser.component';
import {OvertyperComponent} from './overtyper/overtyper.component';
import {ReaderComponent} from './reader/reader.component';
import {ResultsComponent} from './results/results.component';
import {TimerComponent} from './timer/timer.component';
import {TyperComponent} from './typer/typer.component';
import {WelcomeComponent} from './welcome/welcome.component';

// Colleagues use these services.
import {TypingService} from './typing.service';
import {UxStateService} from './ux-state.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('Mediator', () => {
  /* Harnesses for colleagues of the Mediator. */
  let subject: Mediator;
  let chooserHarness: ComponentFixture<ChooserComponent>;
  let timerHarness: ComponentFixture<TimerComponent>;
  let readerHarness: ComponentFixture<ReaderComponent>;
  let overtyperHarness: ComponentFixture<OvertyperComponent>;

  /* Colleagues of the Mediator. */
  let chooser: ChooserComponent;
  let timer: TimerComponent;
  let reader: ReaderComponent;
  let overtyper: OvertyperComponent;

  beforeEach(async () => {
    /* Building the module for the local test environment. */
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [
        ChooserComponent,
        TimerComponent,
        ReaderComponent,
        OvertyperComponent
      ],
      providers: [
        TypingService,
        UxStateService
      ]
    })
      .compileComponents();

    /* Getting components used by Mediator, and their test harnesses. */
    chooserHarness = TestBed.createComponent(ChooserComponent);
    chooser = chooserHarness.componentInstance;

    timerHarness = TestBed.createComponent(TimerComponent);
    timer = timerHarness.componentInstance;

    readerHarness = TestBed.createComponent(ReaderComponent);
    reader = readerHarness.componentInstance;

    overtyperHarness = TestBed.createComponent(OvertyperComponent);
    overtyper = overtyperHarness.componentInstance;
  });

  it('should be created', () => {
    let service = new TypingService(new HttpClient(null));
    subject = new Mediator(service, chooser, timer, overtyper);
    expect(subject).toBeTruthy();
  });

  it(
    'should set service\'s text on overtyper and reader when chooser name event is raised', () => {
      /* Prepare. */
      let arg = 'TestTextName';
      let expected = `Text of ${arg}.`;

      // Service with its dependency method spoofed.
      let service = new TypingService(new HttpClient(null));
      service.getWholeTextByName = (name: string) => {
        return Of({ name: name, text: expected });
      };

      subject = new Mediator(service, chooser, timer, reader, overtyper);

      /* Enact. */
      chooser.onNameChosen.emit(arg);
      chooserHarness.detectChanges();
      readerHarness.detectChanges();
      overtyperHarness.detectChanges();

      /* Compare. */
      expect(reader.textToType).toBe(expected);
      expect(overtyper.textToType).toBe(expected);
    });


  it(
    'should set service\'s text on overtyper and reader when timer time event is raised', () => {
      /* Prepare. */
      let arg = 2;
      let expected = `Text of ${arg}.`;

      // Service with its dependency method spoofed.
      let service = new TypingService(new HttpClient(null));
      service.getRandomByLength = (minutes: number) => {
        return Of({
          name: expected,
          text: expected,
          wordCount: 3,
          minuteLength: arg
        });
      };

      subject = new Mediator(service, chooser, timer, reader, overtyper);

      /* Enact. */
      timer.onTimeButtonClick.emit(arg);
      timerHarness.detectChanges();
      readerHarness.detectChanges();
      overtyperHarness.detectChanges();

      /* Compare. */
      expect(reader.textToType).toBe(expected);
      expect(overtyper.textToType).toBe(expected);
    });
});

