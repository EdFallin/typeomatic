/**/

import { TypeStyle } from "./type-style";

export type UxState = {
  isWelcomeHidden: boolean,
  isTimerHidden: boolean,
  isChooserHidden: boolean,
  typingStyle: TypeStyle
}
