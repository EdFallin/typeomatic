/**/

export type ResultBundle = {
  typed: number,
  minutes: number,
  characters: number,
  typos: number,
  mistakes: number
}

