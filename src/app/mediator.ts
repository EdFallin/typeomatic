/**/

// The Mediator has to know all its colleagues' types.
import {ChooserComponent} from './chooser/chooser.component';
import {OvertyperComponent} from './overtyper/overtyper.component';
import {ReaderComponent} from './reader/reader.component';
import {ResultsComponent} from './results/results.component';
import {TimerComponent} from './timer/timer.component';
import {TyperComponent} from './typer/typer.component';
import {WelcomeComponent} from './welcome/welcome.component';
import { TimeEnderComponent } from "./time-ender/time-ender.component";

import {TypingService} from './typing.service';
import {Text} from './text';

export class Mediator {
  // Colleague members, each a service or component.
  service: TypingService;
  chooser: ChooserComponent;
  overtyper: OvertyperComponent;
  reader: ReaderComponent;
  results: ResultsComponent;
  timer: TimerComponent;
  typer: TyperComponent;
  welcome: WelcomeComponent;
  ender: TimeEnderComponent;

  constructor(...colleagues) {
    /* Setting up colleagues for mediation. */
    let pairings = this.initTypeDestinationPairings();

    for (let colleague of colleagues) {
      this.assignColleagueToDestinationByType(pairings, colleague);
    }

    /* Setting up the actual mediating. */
    this.wireInteractions();
  }

  initTypeDestinationPairings() {
    let pairings = [
      {type: TypingService, destination: 'service'},
      {type: ChooserComponent, destination: 'chooser'},
      {type: OvertyperComponent, destination: 'overtyper'},
      {type: ReaderComponent, destination: 'reader'},
      {type: ResultsComponent, destination: 'results'},
      {type: TimerComponent, destination: 'timer'},
      {type: TyperComponent, destination: 'typer'},
      {type: WelcomeComponent, destination: 'welcome'},
      {type: TimeEnderComponent, destination: "ender" }
    ];

    return pairings;
  }

  assignColleagueToDestinationByType(pairings, colleague) {
    for (let {type, destination} of pairings) {
      if (colleague instanceof type) {
        this[destination] = colleague;
      }
    }
  }

  wireInteractions() {
    this.wireChooserOnNameChosen();
    this.wireTimerOnTimeChosen();
    this.wireOvertyperOnTimeWasCompleted();
  }

  wireChooserOnNameChosen() {
    this.chooser.onNameChosen.subscribe((name: string) => {
      this.service.getWholeTextByName(name)
        .subscribe((text: Text) => {
          this.overtyper.whenTextIsAvailable(text.text, 0);
          this.reader.whenTextIsAvailable(text.text, 0);
        });
    });
  }

  wireTimerOnTimeChosen() {
    this.timer.onTimeButtonClick.subscribe((time) => {
      this.service.getRandomByLength(time)
        .subscribe((typeable) => {
          this.overtyper.whenTextIsAvailable(typeable.text, typeable.minuteLength);
          this.reader.whenTextIsAvailable(typeable.text, typeable.minuteLength);
        });
    });
  }

  wireOvertyperOnTimeWasCompleted() {
    this.overtyper.onTimeWasCompleted.subscribe((results) => {
      this.results.calculateResults(results);
      this.ender.shouldBeDisplayed = true;
    });
  }
}
