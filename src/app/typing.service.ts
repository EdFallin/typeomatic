/**/

import { Injectable } from "@angular/core";
import { Output, EventEmitter } from "@angular/core";
import { Typeable } from "./typeable";
import { Text } from "./text";
import { HttpClient } from "@angular/common/http";
import { Observable, of as Of } from "rxjs";

@Injectable({ providedIn : "root" })
export class TypingService {
  ROOT = "https://TypeOMaticServer.azurewebsites.net/Typing";
  /*
    Get:
      √/Typing/MinutesPossible
      √/Typing/Random/{minutes}
      √/Typing/Random/{name}/{minutes}
      √/Typing/All/Pieces
      √/Typing/WholeNamed/{name}
      √/Typing/All/Names
      √/Typing/All/Texts
      ( /UxState )

    Post:
      /Typing/Add/{name}
      ( /UxState )

    Delete:
      /Typing/Delete/{name}
   */

  constructor(private http : HttpClient) {
    /* No operations. */
  }

  getMinutesPossible() : Observable<number[]> {
    return this.http.get<number[]>(`${ this.ROOT }/MinutesPossible`);
  }

  getRandomByLength(minutes: number) : Observable<Typeable> {
    return this.http.get<Typeable>(`${ this.ROOT }/Random/${ minutes }`);
  }

  getRandomNamedByLength(name: string, minutes: number) : Observable<string[]> {
    return this.http.get<string[]>(`${ this.ROOT }/Random/${ name }/${ minutes }`);
  }

  getAllPieces() : Observable<Typeable[]> {
    return this.http.get<Typeable[]>(`${this.ROOT}/All/Pieces`);
  }

  getWholeTextByName(name: string) : Observable<Text> {
    return this.http.get<Text>(`${ this.ROOT }/WholeNamed/${ name }`);
  }

  getAllNames() : Observable<string[]> {
    return this.http.get<string[]>(`${this.ROOT}/All/Names`);
  }

  getAllTexts() : Observable<Text[]> {
    return this.http.get<Text[]>(`${this.ROOT}/All/Texts`);
  }
}
